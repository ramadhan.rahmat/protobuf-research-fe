import React, { FC, useEffect } from "react";
import StocksSchema from "../../../protos/stocks_pb";

const withWebsocket = (WrappedComponent: FC): FC => {
  const HOCWrapper: FC = () => {
    useEffect(() => {
      const socket = new WebSocket("ws://localhost:5001/websockets");
      socket.binaryType = "arraybuffer";

      // Connected to server
      socket.addEventListener("open", (evt) => {
        console.log("[Websocket - Open] Connected", { evt, StocksSchema });

        // Sample of sending data to server
        const message = new (StocksSchema as any).Stock();
        message.setId(1);
        message.setName("BBCA [Websocket]");
        message.setPrice(7001);

        console.log("[Websocket - Open] Created message", {
          message,
        });

        const messageBuffer = message.serializeBinary();

        socket.send(messageBuffer);

        console.log("[Websocket - Open] Message sent", {
          messageBuffer: messageBuffer.toString(),
        });
      });

      socket.addEventListener("message", (evt) => {
        // Direct log if message is string
        if (typeof evt.data === "string")
          return console.log("[Websocket - Message] Received string message", {
            message: evt.data,
          });

        // Logging available schema
        console.log("[Websocket - Message] Checking available schema", {
          StocksSchema,
        });

        // Deserializing received data
        const deserializedData = (
          StocksSchema as any
        ).StockList.deserializeBinary(evt.data);

        console.log("[Websocket - Message] Deserialized message", {
          deserializedData,
          data: evt.data,
        });
      });
    }, []);

    return <WrappedComponent />;
  };

  HOCWrapper.displayName = "withWebsocketWrapper";

  return HOCWrapper;
};

export default withWebsocket;
