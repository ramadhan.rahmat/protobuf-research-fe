import { FC, useEffect } from "react";
import protobuf from "protobufjs";

const withProtobufJsWebscoket = (WrappedComponent: FC): FC => {
  const HOCWrapper: FC = () => {
    useEffect(() => {
      console.log("[Protobufjs - Open] Loading stocks.proto...");
      protobuf.load("/protos/stocks.proto", (err, root) => {
        const socket = new WebSocket("ws://localhost:5001/websockets");
        socket.binaryType = "arraybuffer";
        console.log("[Protobufjs - Open] Received proto file", {
          root,
        });

        if (err) console.error("[Protobufjs - Open] Error", { err });

        const stockType = root?.lookupType("Stock");
        const stockListType = root?.lookupType("StockList");

        if (!stockType || !stockListType)
          return console.error("[Protobufjs - Open] Lookup type empty!", {
            stockType,
            stockListType,
          });

        console.log("[Protobufjs - Open] Received type", { stockType });

        // Connected to server
        socket.addEventListener("open", (evt) => {
          console.log("[Protobufjs - Open] Connected", { evt });

          const stockMessage = stockType.create({
            id: 3,
            name: "BBCA (dari protobufjs)",
            price: 7003,
          });

          console.log("[Protobufjs - Open] Created message", {
            stockMessage,
          });

          const verifyMessage = stockType.verify(stockMessage);

          if (verifyMessage)
            console.error(
              "[Protobufjs - Open] Failed to verify message",
              verifyMessage
            );

          const stockMessageBuffer = stockType.encode(stockMessage).finish();

          socket.send(stockMessageBuffer);
          console.log("[Protobufjs - Open] Message sent", {
            stockMessageBuffer: stockMessageBuffer.toString(),
          });
        });

        socket.addEventListener("message", (evt) => {
          // Direct log if message is string
          if (typeof evt.data === "string")
            return console.log(
              "[Protobufjs - Message] Received string message",
              {
                message: evt.data,
              }
            );

          console.log("[Protobufjs - Message] Received event message", {
            message: evt.data,
          });

          console.log("[Protobufjs - Message] Checking available schema", {
            stockType,
            root,
          });

          // Deserializing received data
          const deserializedData = stockListType.decode(
            new Uint8Array(evt.data)
          );

          console.log("[Protobufjs - Message] Deserialized message", {
            deserializedData,
            data: evt.data,
          });
        });
      });
    }, []);

    return <WrappedComponent />;
  };

  HOCWrapper.displayName = "withProtobufJsWebsocketWrapper";
  return HOCWrapper;
};

export default withProtobufJsWebscoket;
