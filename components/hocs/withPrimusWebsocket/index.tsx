import { FC, useEffect, useRef } from "react";
import Schema from "../../../protos/stocks_pb";

const withPrimusWebsocket = (WrappedComponent: FC): FC => {
  const HOCWrapper: FC = () => {
    useEffect(() => {
      const primus = new Primus("ws://localhost:7001/primus");

      // Will execute callback when connected to primus ws server
      primus.on("open", () => {
        console.log("[Primus - Open] Connected");

        // Resending new data to the server
        const message = new (Schema as any).Stock();
        message.setId(2);
        message.setName("BBCA [Primus]");
        message.setPrice(7002);

        console.log("[Primus - Open] Created message", {
          message,
        });

        const messageBuffer = message.serializeBinary();

        primus.write(message.serializeBinary());

        console.log("[Primus - Open] Message sent", {
          messageBuffer: messageBuffer.toString(),
        });
      });

      // Will execute callback when receiving any data from the server
      primus.on("data", (data: any) => {
        // Direct log if message is string
        if (typeof data === "string")
          return console.log("[Primus - Message] Received string", {
            message: data,
          });

        // Logging available schema
        console.log("[Primus - Message] Checking available schema", {
          Schema,
        });

        // Deserializing recieved Data
        const deserializedData = (Schema as any).StockList.deserializeBinary(
          data
        );

        console.log("[Primus - Message] Deserialized message", {
          data,
          deserializedData,
        });
      });
    }, []);

    return <WrappedComponent />;
  };

  HOCWrapper.displayName = "withPrimusWebsocketWrapper";

  return HOCWrapper;
};

export default withPrimusWebsocket;
