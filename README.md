# Protobuf Websocket Integration

## Overview

This project cover research about protobuf integration, project built with **Typescript, Next Js with custom server using Express, and Primus.**

This README file contains how to run project, integration issues, and research results

## Quick Guide

- [Protobuf Repository](https://github.com/protocolbuffers/protobuf)
- [Primus Repository](https://github.com/primus/primus)
- [JSON and Protobuf](https://www.youtube.com/watch?v=uGYZn6xk-hA)
- [Protobuf Javascript Implementation (Separated from main Protobuf repo at 28 Apr 2022)](https://github.com/protocolbuffers/protobuf-javascript)

## Quick Start

### `yarn dev`

for starting nodemon server that will execute `./server/index.ts` since we are using custom server with next js

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

- Nodejs websocket available at port `5000`
    - Node js websocket routes at `ws://localhost:5000/websockets`
- Primus websocket available at port `7000`
    - Primus websocket routes at `ws://localhost:7000/primus`

## Implementation

### Nodejs Websocket Implementation

**Client**

- Implementation of client websocket connection: `./components/hocs/withWebsocket/index.tsx`

**Server**

- Websocket server init: `./server/index.ts:32`
- Websocket instance implementation: `./server/websockets.ts`

### Primus Websocket Implementation

**Client**

- Primus js client script link: `./pages/primus.tsx:15`
- Implementation of client primus connection: `./components/hocs/withPrimusWebsocket/index.tsx`

**Server**

- Primus instance implementation: `./server/index.ts:50`

## Message Size

Size difference between JSON string message, and protobuf of 100 entries of Stocks





1. JSON string: `3.79 KB`
![1_protobuf](/uploads/10daacdbc2a8ff481c7909dd1d2fde03/1_protobuf.png) 

2. Protobuf: `1.65 KB`
![2_protobuf](/uploads/f1266fedb39c6f298ed225bbb611d217/2_protobuf.png)

Sample data:

```jsx
[{"id":1,"name":"BBCA1","price":1000},{"id":2,"name":"BBCA2","price":1000},{"id":3,"name":"BBCA3","price":1000},{"id":4,"name":"BBCA4","price":1000},{"id":5,"name":"BBCA5","price":1000},{"id":6,"name":"BBCA6","price":1000},{"id":7,"name":"BBCA7","price":1000},{"id":8,"name":"BBCA8","price":1000},{"id":9,"name":"BBCA9","price":1000},{"id":10,"name":"BBCA10","price":1000},{"id":11,"name":"BBCA11","price":1000},{"id":12,"name":"BBCA12","price":1000},{"id":13,"name":"BBCA13","price":1000},{"id":14,"name":"BBCA14","price":1000},{"id":15,"name":"BBCA15","price":1000},{"id":16,"name":"BBCA16","price":1000},{"id":17,"name":"BBCA17","price":1000},{"id":18,"name":"BBCA18","price":1000},{"id":19,"name":"BBCA19","price":1000},{"id":20,"name":"BBCA20","price":1000},{"id":21,"name":"BBCA21","price":1000},{"id":22,"name":"BBCA22","price":1000},{"id":23,"name":"BBCA23","price":1000},{"id":24,"name":"BBCA24","price":1000},{"id":25,"name":"BBCA25","price":1000},{"id":26,"name":"BBCA26","price":1000},{"id":27,"name":"BBCA27","price":1000},{"id":28,"name":"BBCA28","price":1000},{"id":29,"name":"BBCA29","price":1000},{"id":30,"name":"BBCA30","price":1000},{"id":31,"name":"BBCA31","price":1000},{"id":32,"name":"BBCA32","price":1000},{"id":33,"name":"BBCA33","price":1000},{"id":34,"name":"BBCA34","price":1000},{"id":35,"name":"BBCA35","price":1000},{"id":36,"name":"BBCA36","price":1000},{"id":37,"name":"BBCA37","price":1000},{"id":38,"name":"BBCA38","price":1000},{"id":39,"name":"BBCA39","price":1000},{"id":40,"name":"BBCA40","price":1000},{"id":41,"name":"BBCA41","price":1000},{"id":42,"name":"BBCA42","price":1000},{"id":43,"name":"BBCA43","price":1000},{"id":44,"name":"BBCA44","price":1000},{"id":45,"name":"BBCA45","price":1000},{"id":46,"name":"BBCA46","price":1000},{"id":47,"name":"BBCA47","price":1000},{"id":48,"name":"BBCA48","price":1000},{"id":49,"name":"BBCA49","price":1000},{"id":50,"name":"BBCA50","price":1000},{"id":51,"name":"BBCA51","price":1000},{"id":52,"name":"BBCA52","price":1000},{"id":53,"name":"BBCA53","price":1000},{"id":54,"name":"BBCA54","price":1000},{"id":55,"name":"BBCA55","price":1000},{"id":56,"name":"BBCA56","price":1000},{"id":57,"name":"BBCA57","price":1000},{"id":58,"name":"BBCA58","price":1000},{"id":59,"name":"BBCA59","price":1000},{"id":60,"name":"BBCA60","price":1000},{"id":61,"name":"BBCA61","price":1000},{"id":62,"name":"BBCA62","price":1000},{"id":63,"name":"BBCA63","price":1000},{"id":64,"name":"BBCA64","price":1000},{"id":65,"name":"BBCA65","price":1000},{"id":66,"name":"BBCA66","price":1000},{"id":67,"name":"BBCA67","price":1000},{"id":68,"name":"BBCA68","price":1000},{"id":69,"name":"BBCA69","price":1000},{"id":70,"name":"BBCA70","price":1000},{"id":71,"name":"BBCA71","price":1000},{"id":72,"name":"BBCA72","price":1000},{"id":73,"name":"BBCA73","price":1000},{"id":74,"name":"BBCA74","price":1000},{"id":75,"name":"BBCA75","price":1000},{"id":76,"name":"BBCA76","price":1000},{"id":77,"name":"BBCA77","price":1000},{"id":78,"name":"BBCA78","price":1000},{"id":79,"name":"BBCA79","price":1000},{"id":80,"name":"BBCA80","price":1000},{"id":81,"name":"BBCA81","price":1000},{"id":82,"name":"BBCA82","price":1000},{"id":83,"name":"BBCA83","price":1000},{"id":84,"name":"BBCA84","price":1000},{"id":85,"name":"BBCA85","price":1000},{"id":86,"name":"BBCA86","price":1000},{"id":87,"name":"BBCA87","price":1000},{"id":88,"name":"BBCA88","price":1000},{"id":89,"name":"BBCA89","price":1000},{"id":90,"name":"BBCA90","price":1000},{"id":91,"name":"BBCA91","price":1000},{"id":92,"name":"BBCA92","price":1000},{"id":93,"name":"BBCA93","price":1000},{"id":94,"name":"BBCA94","price":1000},{"id":95,"name":"BBCA95","price":1000},{"id":96,"name":"BBCA96","price":1000},{"id":97,"name":"BBCA97","price":1000},{"id":98,"name":"BBCA98","price":1000},{"id":99,"name":"BBCA99","price":1000},{"id":100,"name":"BBCA100","price":1000}]
```

## Current Issue

- Can’t run Primus when using same version as web legacy project `6.0.8` 

- Implementation 1:
    
    ```
    const primus = new Primus(server);
    primus.library();
    ```
    
    Received Error `TypeError: Cannot read properties of undefined (reading 'headers')`
    
    - Implementation 2
    
    ```
     const primus = Primus.createServer({
          headers: false,
        });
     primus.library();
    ```
    
    Received Error `Error: Missing the SSL key or certificate files in the options.`
    
    This could solved by upgrading primus to latest version / higher version that `6.0.8` , should be still fine for research purpose since the implementation is close to same
    

- Typescript Typing for Primus and Protobuf weren’t available since both script files received by network, solution is either casting type manually to a variable, or defining it from globals definition file.
