import express, { Request, Response } from "express";
import next from "next";
import websockets from "./websockets";
import Primus from "primus";
import Schema from "../protos/stocks_pb";
import { getJsonStockCollection, getStockCollection } from "./util";

const dev = process.env.NODE_ENV !== "production";
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();
const port = process.env.PORT || 3001;
const wsPort = process.env.WSPORT || 5001;
const wsPrimusPort = process.env.PRIMUSPORT || 7001;

(async () => {
  try {
    await nextApp.prepare();
    const app = express();
    app.all("*", (req: Request, res: Response) => handle(req, res));
    app.listen(port, (err?: any) => {
      if (err) throw err;
      console.log(`> Ready on localhost:${port} - env ${process.env.NODE_ENV}`);
    });
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();

// Nodejs Websocket setup available at port {wsPort}
// Example of sending protobuf message available at ./websocket.ts
(async () => {
  try {
    const app = express();
    const server = app.listen(wsPort, (err?: any) => {
      if (err) throw err;
      console.log(
        `> Express Websocket Ready on localhost:${wsPort} - env ${process.env.NODE_ENV}`
      );
    });

    websockets(server);
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();

// Primus websockets setup available at port {wsPrimusPort}
(async () => {
  try {
    const app = express();
    const server = app.listen(wsPrimusPort, (err?: any) => {
      if (err) throw err;
      console.log(
        `> Primus Ready on localhost:${wsPrimusPort} - env ${process.env.NODE_ENV}`
      );
    });

    const parser = {
      encoder: function encoder(data: any, fn: any) {
        fn(undefined, data);
      },
      decoder: function decoder(data: any, fn: any) {
        fn(undefined, data);
      },
    };

    const primus = new Primus(server, { transformer: "websockets", parser });
    primus.library();

    primus.on("connection", (spark) => {
      console.log("Connected to primus server");
      spark.write(getStockCollection().serializeBinary());
      spark.write(JSON.stringify(getJsonStockCollection()));

      spark.on("data", (message) => {
        console.log(`Data ${message}`);
        console.log(new (Schema as any).Stock.deserializeBinary(message));
      });
    });
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();
