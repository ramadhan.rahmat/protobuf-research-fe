import Schema from "../protos/stocks_pb";

export const getStockCollection = (count = 100) => {
  const stockCollection = new (Schema as any).StockList();
  for (let i = 1; i <= count; i += 1) {
    const stock = new (Schema as any).Stock();
    stock.setId(i);
    stock.setName(`BBCA${i}`);
    stock.setPrice(1000);

    stockCollection.addStocks(stock);
  }

  return stockCollection;
};

export const getJsonStockCollection = (count = 100) => {
  const stockCollection = [];
  for (let i = 1; i <= count; i += 1) {
    stockCollection.push({
      id: i,
      name: `BBCA${i}`,
      price: 1000,
    });
  }

  return stockCollection;
};
