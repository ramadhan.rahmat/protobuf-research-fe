import { Server } from "http";
import QueryString from "qs";
import WebSocket from "ws";
import { getJsonStockCollection, getStockCollection } from "./util";
import Schema from "../protos/stocks_pb";

const WebsocketServer = (expressServer: Server) => {
  const websocketServer = new WebSocket.Server({
    noServer: true,
    path: "/websockets",
  });

  expressServer.on("upgrade", (req, socket, head) => {
    websocketServer.handleUpgrade(req, socket, head, (websocket: WebSocket) => {
      websocketServer.emit("connection", websocket, req);
    });
  });

  websocketServer.on("connection", (websocketConnection, connectionRequest) => {
    console.log("Connected to express websocket server");
    const [_path, params] = connectionRequest?.url?.split("?") || [];
    const wsParams = QueryString.parse(params);

    if (Object.keys(wsParams).length > 0) console.log(wsParams);

    websocketConnection.send(getStockCollection().serializeBinary());
    websocketConnection.send(JSON.stringify(getJsonStockCollection()));

    websocketConnection.on("message", (message) => {
      console.log(message);
      const deserializedData = new (Schema as any).Stock.deserializeBinary(
        message
      );
      console.log({ deserializedData: deserializedData.array });
    });
  });

  return websocketServer;
};

export default WebsocketServer;
